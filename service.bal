import ballerina/graphql;

# Here we decided to define the data types we will use through out the code
# 
# + region - Region we will be viewing
# + deaths - Number of deaths
# + confirmed_cases - Number of confirmed cases  
# + recoveries - Number of recoveries recorded  
# + tested - Number of people that have been tested
public type Cov19Entry record {|readonly string region;
decimal deaths?;
decimal confirmed_cases?;
decimal recoveries?;
decimal tested?;
|};

table<Cov19Entry> key(region) cov19EntriesTable = table[
    {region: "Khomas", deaths: 39, confirmed_cases: 465, recoveries: 67, tested: 1200},
    {region: "Caprivi", deaths: 16, confirmed_cases: 286, recoveries: 32, tested: 1310},
    {region: "Erongo", deaths: 48, confirmed_cases: 436, recoveries: 94, tested: 2430},
    {region: "Kunene", deaths: 35, confirmed_cases: 345, recoveries: 69, tested: 1470},
    {region: "Kavango West", deaths: 42, confirmed_cases: 230, recoveries: 54, tested: 2145},
    {region: "Kavango East", deaths: 70, confirmed_cases: 643, recoveries: 56, tested: 3012},
    {region: "Ohangwana", deaths: 49, confirmed_cases: 327, recoveries: 76, tested: 1278},
    {region: "Omaheke", deaths: 34, confirmed_cases: 484, recoveries: 84, tested: 2001},
    {region: "Omusati", deaths: 67, confirmed_cases: 523, recoveries: 90, tested: 1988},
    {region: "Oshana", deaths: 43, confirmed_cases: 364, recoveries: 67, tested: 4000},
    {region: "Oshikoto", deaths: 52, confirmed_cases: 268, recoveries: 78, tested: 1890},
    {region: "Otjozondjupa", deaths: 37, confirmed_cases: 395, recoveries: 50, tested: 2345},
    {region: "Hardap", deaths: 24, confirmed_cases: 421, recoveries: 66, tested: 2500},
    {region: "Karas", deaths: 46, confirmed_cases: 467, recoveries: 78, tested: 1589}
];

# Here we defined the mutation type to and data into the code
public distinct service class Cov19Data {
    private final readonly & Cov19Entry entryRecord;

    function init(Cov19Entry entryRecord) {
        self.entryRecord = entryRecord.cloneReadOnly();
        
    }

    resource function get region () returns string {
        return self.entryRecord.region;
        
    }
    resource function get deaths () returns decimal? {
        if self.entryRecord.deaths is decimal {
            return self.entryRecord.deaths / 1000;
            
        }
        return;
    }
    resource function get confirmed_cases () returns decimal? {
        if self.entryRecord.confirmed_cases is decimal {
            return self.entryRecord.confirmed_cases / 1000;
            
        }
        return;
    }
       resource function get recoveries () returns decimal? {
        if self.entryRecord.recoveries is decimal {
            return self.entryRecord.recoveries / 1000;

        }
        return;
    }    
        resource function get tested () returns decimal? {
        if self.entryRecord.tested is decimal {
            return self.entryRecord.tested / 1000;
            
        }
        return;

    }    
    
}

service /covid19 on new graphql:Listener(9000) {
    resource function get all() returns Cov19Data[] {
        Cov19Entry[] cov19Entries = cov19EntriesTable.toArray().cloneReadOnly();
        return cov19Entries.'map(entry = new Cov19Data(null));
    }

    resource function get filter (string region) returns Cov19Data? {
        Cov19Entry? cov19Entry = cov19EntriesTable[region];
        if cov19Entry is Cov19Entry {
            return new (cov19Entry);
            
        }
        return;
        
    }

    remote function add(Cov19Entry entry) returns Cov19Data {
        cov19EntriesTable.add(entry);
        return new Cov19Data(entry);
        
    }
    
}
