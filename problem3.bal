   import ballerina/http
;
   service /student/details on new http:Listener(9000){

    resource function get name() returns problem_3Entry[] {
        return problem_3Table.toArray();

    }

    resource function post name(@http:Payload problem_3Entry[] problem_3Entries)
   returns CreatedProblem_3Entries|Conflictingst_numbers {

    decimal[] conflictingst_numbers = from problem_3Entry problem_3Entry in problem_3Entries
    where problem_3Table.hasKey(problem_3Entry.st_number)
    select problem_3Entry.course_code;

    if Conflictingst_numbers.length() > 0 {
        return {
            body: {
                errmsg: string:'join(" ", "Conflicting Student Number:", ...conflictingst_numbers)
            }
        };

        
    }else {
        problem_3Entries.forEach(problem_3Entry => problem_3Table.add(problem_3Entry));
        return <CreatedProblem_3Entries>{body: problem_3Entries};
    }
        
}

resource function get name/[decimal st_number]() returns problem_3Entry {
        problem_3Entry? problem_3Entry = problem_3Table[st_number];
        if problem_3Entry is () {
            return {
                body: {
                    errmsg: string `Invalid Student Number: ${st_number}`
                }
            };
        }
        return problem_3Entry;
    }
}

    # Here we detail the fields we will need in our table
    #
    # + name - Full name of students.  
    # + st_number - student number declaration  
    # + course - course being attended
    # + course_code - course code 
    # + email - students email
    public type problem_3Entry record{|
    readonly decimal st_number;
    string name;
    string course;
    string course_code;
    string email;
|};
public final table<problem_3Entry> key(st_number) problem_3Table = table [
    {name: "Max Aarons", st_number: 10006, course: "Data and Analytics", course_code: "DA1", email:"maxaarons@gmail.com" },
    {name: "Tom Shelby", st_number: 10012, course: "Data and Analytics", course_code: "DA1", email:"tomshelby09@yahoo.com" },
    {name: "Ona Mazi", st_number: 10024, course: "Data and Analytics", course_code: "DA1", email:"onamazi00@gmail.com" },
]; 

# here we define the record named CreatedProblem_3Entries, it will allow the delcared to expect a payload from problem_3Entry
#
# + body - Field Description
public type CreatedProblem_3Entries record {|
*http:Created;
problem_3Entry[] body;
|};

# here we define the record named Conflictingst_numbers, it will allow the delcared to expect a payload and then return the desired values
#
# + body - Field Description
public type Conflictingst_numbers record {|
*http:Conflict;

ErrorMsg body;
|};

# Conflictingst_numbers will return a message if the value inserted is not correct or invalid
#
# + body - is the error message
public type InvalidSt_numberError record {|
*http:NotFound;
ErrorMsg body;
|};

# Prints the error message
#
# + errmsg - Field Description
public type ErrorMsg record {|
string errmsg;
|};

